import Foundation

/// Within the VitalsService all progresses dealing with the vitals extraction are made
class DateService{
    
    /// Format a date to the given date pattern
    ///
    /// - Parameter fromDate: the date
    /// - Returns: the formatted date
    static func getFormattedDate(fromDate: Date) -> String{
        return getDateFormatter().string(from: fromDate)
    }
    
    /// Format a date string to the given date pattern
    ///
    /// - Parameter fromString: the date string
    /// - Returns: the formatted date
    static func getFormattedDate(fromString: String) -> Date{
        return getDateFormatter().date(from: fromString)!
    }
    
    /// Get a DateFormatter with preset date pattern
    ///
    /// - Returns: the DateFormatter
    private static func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateEnum.pattern_long.rawValue
        
        return dateFormatter
    }
    
    /// Set the start date for heart rate fetching
    ///
    /// - Parameter forDate: the start date
    static func setStartDate(forDate: Date){
        let startDate = UserDefaults.standard.object(forKey: "startDate")
        if((startDate) != nil){
            UserDefaults.standard.set(startDate, forKey: "lastStartDate")
        }
        UserDefaults.standard.set(forDate, forKey: "startDate")
    }
    
    /// Reset the start date for heart rate fetching
    static func resetStartDate(){
        let lastStartDate = UserDefaults.standard.object(forKey: "lastStartDate")
        if((lastStartDate) != nil){
            UserDefaults.standard.set(lastStartDate, forKey: "startDate")
        }
    }
    
    /// Getting the current start date
    ///
    /// - Returns: the start date
    static func getStartDate() -> Date {
        var startDate:Date? = UserDefaults.standard.object(forKey: "startDate") as? Date
        if(startDate == nil){
            startDate = getStartofDay()
        }
        return startDate!
    }
    
    /// Getting current date
    ///
    /// - Returns: the date
    static func getCurrentDate() -> Date {
        let currentDateString = Date().description(with: .current)
        let dateFormatter: DateFormatter = DateFormatter()
        return dateFormatter.date(from: currentDateString) ?? Date()
    }
    
    /// Getting midnight date of current date
    ///
    /// - Returns: the date
    static func getStartofDay() -> Date {
        let calendar = Calendar.current
        return calendar.startOfDay(for: getCurrentDate())
    }
}
