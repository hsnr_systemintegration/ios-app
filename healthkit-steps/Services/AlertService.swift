import Foundation
import UIKit

class AlertService{
    
    /// Generates an alert box
    ///
    /// - Parameters:
    ///   - title: the title of the alert
    ///   - message: the message of the alert
    ///   - viewController: the instance of the ViewController of the app
    static func generateSimpleAlert(title: String, message:String, viewController:ViewController){
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
}
