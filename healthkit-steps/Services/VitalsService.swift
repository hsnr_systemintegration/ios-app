import UIKit
import HealthKit
import Foundation
import SMART
import os.log

/// Within the VitalsService all progresses dealing with the vitals extraction are made
class VitalsService{
    
    let healthStore:HKHealthStore
    let fhirService = FHIRService()
    
    init(healthStore: HKHealthStore){
        self.healthStore = healthStore
    }
    
    /// Before the vitals data can be used the user has to give read permissions
    func authoriseHealthKitAccess() {
        let healthKitTypes: Set = [
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        ]
        self.healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (_, _) in
        }
        self.healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
            if let e = error {
                DateService.resetStartDate()
                os_log(LoggingStrings.AUTHORIZATION_FAILED, log: OSLog.default, type: .error, e.localizedDescription)
            } else {
                os_log(LoggingStrings.AUTHORIZATION_SUCCESS, log: OSLog.default, type: .info)
            }
        }
    }
    
    /// Getting todays steps
    ///
    /// - Parameter completion: the callback called when the steps are fetched
    func getTodaysSteps(completion: @escaping (Double) -> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        var resultCount = 0.0
        let predicate = HKQuery.predicateForSamples(withStart: DateService.getStartofDay(), end: DateService.getCurrentDate(), options: .strictStartDate)
        print(DateService.getStartDate())
        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            
            guard let result = result else {
                DateService.resetStartDate()
                print("Failed to fetch steps rate")
                completion(resultCount)
                return
            }
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.count())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        self.healthStore.execute(query)
    }
    
    /// Getting all heart rates that are now already sent to the OpenEMR server.
    ///
    /// - Parameter viewController: the instance of the ViewController of the app
    func fetchVitals(viewController:ViewController)
    {
        let heartRateUnit:HKUnit = HKUnit(from: "count/min")
        let heartRateType:HKQuantityType   = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        var heartRateQuery:HKSampleQuery?
        
        let startDate = DateService.getStartDate()
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: DateService.getCurrentDate(), options: [])
        let sortDescriptors = [
            NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        ]
        
        heartRateQuery = HKSampleQuery(sampleType: heartRateType,
                                       predicate: predicate,
                                       limit: 25,
                                       sortDescriptors: sortDescriptors)
        { (query:HKSampleQuery, heartRates:[HKSample]?, error:Error?) -> Void in
            
            guard error == nil else {
                DateService.resetStartDate()
                print("error"); return }
            
            self.getTodaysSteps { (steps) in
                DispatchQueue.main.async {
                    
                    if(heartRates?.isEmpty != true){
                        self.processData(heartRates: heartRates!, heartRateUnit: heartRateUnit, steps: steps, viewController:viewController)
                    } else {
                        AlertService.generateSimpleAlert(title: AlertEnum.title_info.rawValue, message: AlertEnum.message_info_uptodate.rawValue, viewController:viewController)
                    }
                }
            }
        }
        self.healthStore.execute(heartRateQuery!)
    }
    
    /// Build an array with all vitals data
    ///
    /// - Parameters:
    ///   - heartRates: all fetched heart rates
    ///   - heartRateUnit: the unit of the heart rates
    ///   - steps: todys steps
    ///   - viewController: the instance of the ViewController of the app
    private func processData(heartRates:Array<HKSample>, heartRateUnit:HKUnit, steps:Double, viewController:ViewController){
        
        var vitalsArr = [] as! Array<Dictionary<String, String>>
        
        for (index, heartRate) in heartRates.enumerated()
        {
            guard let current:HKQuantitySample = heartRate as? HKQuantitySample else { return }
            
            let heartRate = current.quantity.doubleValue(for: heartRateUnit)
            let date = DateService.getFormattedDate(fromDate:  current.startDate)
            
            if(index == 0){
                DateService.setStartDate(forDate: current.startDate)
            }
            
            let currentHashSet = [
                "date" : date,
                "heartRate" : String(Int(heartRate)),
                "steps" : String(Int(steps))
                ] as [String : String]
            
            vitalsArr.append(currentHashSet)
        }
        self.iterateVitals(vitalsArr: vitalsArr, viewController: viewController)
    }
    
    /// Build for each heart rate an observation resource with todays steps and pass it to the RequestService
    ///
    /// - Parameters:
    ///   - vitalsArr: all vitals
    ///   - viewController: the instance of the ViewController of the app
    private func iterateVitals(vitalsArr: Array<Dictionary<String,String>>, viewController:ViewController){
        var pid:String = ""
        var date:String = ""
        var heartRate:String = ""
        var steps:String = ""
        
        vitalsArr.forEach() { measurement in
            
            date = measurement["date"]!
            heartRate = measurement["heartRate"]!
            steps = measurement["steps"]!
        }
        pid = UserDefaults.standard.string(forKey: "pid")!
        
        let observation = fhirService.generateObservationResource(withPid: pid, withDate: date, withHeartRate: heartRate , withSteps: steps)
        doRequest(withObservation: observation, viewController:viewController)
    }
    
    private func doRequest(withObservation: Observation, viewController:ViewController){
        let server = UserDefaults.standard.string(forKey: "server")
        
        if(server != nil){
            let requestService = RequestService(server: server!)
            requestService.sendRequest(withObservation: withObservation, viewController: viewController)
        }
    }
}
