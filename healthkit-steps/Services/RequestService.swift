import Foundation
import UIKit
import SMART
import os.log


/// Within the RequestService all progresses dealing with the HTTP request are made
class RequestService {
    let contentType = "application/fhir+json"
    let acceptType = "application/fhir+json"
    var url:URL
    let fhirService = FHIRService()
    var wasError:Bool
    
    init(server:String){
        self.url = URL(string: server)!
        wasError = false;
    }
    
    /// Sends the HTTP request to the mirth server for pushing the observation resource to th OpenEMR documentation
    ///
    /// - Parameters:
    ///   - withObservation: the observation resource
    ///   - viewController: the instance of the ViewController of the app
    func sendRequest(withObservation: Observation, viewController:ViewController){
        let data = withObservation.debugDescription
        var request = URLRequest(url: self.url)
        request.setValue(self.contentType, forHTTPHeaderField: "Content-Type")
        request.setValue(self.acceptType, forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.httpBody = data.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                os_log(LoggingStrings.REQUEST_ERROR_INTERNAL, log: OSLog.default, type: .fault)
                DateService.resetStartDate()
                AlertService.generateSimpleAlert(title: AlertEnum.title_error.rawValue, message: AlertEnum.message_error_internal.rawValue, viewController: viewController)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                os_log(LoggingStrings.REQUEST_ERROR_CODE, log: OSLog.default, type: .fault)
                DateService.resetStartDate()
                AlertService.generateSimpleAlert(title: AlertEnum.title_error.rawValue, message: AlertEnum.message_error_server.rawValue, viewController: viewController)
            } else {
                os_log(LoggingStrings.REQUEST_SUCCESS, log: OSLog.default, type: .info, withObservation.identifier!)
                AlertService.generateSimpleAlert(title: AlertEnum.title_success.rawValue, message: AlertEnum.message_success.rawValue, viewController: viewController)
            }
        }
        task.resume()
    }
}
