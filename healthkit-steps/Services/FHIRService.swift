import Foundation
import SMART
import os.log

/// Within the FHIRService all progresses dealing with FHIR resources are made
class FHIRService{
    
    /// Generates an heart rate observation resource object with the given data
    ///
    /// - Parameters:
    ///   - withPid: the patient identifier
    ///   - withDate: the dateTime when the observation was made
    ///   - withHeartRate: the heart reate value
    ///   - withSteps: the steps value
    /// - Returns: the generated observation resource object
    func generateObservationResource(withPid: String, withDate: String, withHeartRate: String, withSteps: String) -> Observation{
        
        let observation = Observation()
        
        observation.identifier = [getIdentifer(withValue: FHIREnum.identifer.rawValue)]
        observation.meta = getMeta(withProfile: FHIREnum.meta_profile.rawValue)
        observation.status = ObservationStatus.final
        observation.category = [getCodeableConcept(withSystem: FHIREnum.category_coding_system.rawValue, withCode: FHIREnum.category_coding_code.rawValue, withValue: FHIREnum.category_coding_display.rawValue)]
        observation.code = getCodeableConcept(withSystem: FHIREnum.code_coding_system.rawValue, withCode: FHIREnum.code_coding_code.rawValue, withValue: FHIREnum.code_coding_display.rawValue)
        observation.subject = getReference(withPid: withPid)
        observation.effectiveDateTime = DateTime.init(string: withDate)
        observation.valueQuantity = getValueQuantity(heartRate: withHeartRate, unit: FHIREnum.valueQuantity_unit.rawValue, system: FHIREnum.valueQuantity_system.rawValue, code: FHIREnum.valueQuantity_code.rawValue)
        observation.comment = FHIRString.init(withSteps)
        
        os_log(LoggingStrings.OBSERVATION_CREATED, log: OSLog.default, type: .info, withPid, withHeartRate, withDate, withSteps)
        
        return observation
    }
    
    /// Generates a codable concept object for the observation resource
    ///
    /// - Parameters:
    ///   - withSystem: the value of the system for the codable concept
    ///   - withCode: the value of the code for the codable concept
    ///   - withValue: the value of the description for the codable concept
    /// - Returns: the codeable concept object
    private func getCodeableConcept(withSystem:String, withCode:String, withValue:String) -> CodeableConcept{
        let codeableConcept = CodeableConcept()
        codeableConcept.coding = [getCoding(withSystem: withSystem, withCode: withCode, withDisplay: withValue)]
        codeableConcept.text = FHIRString.init(withValue)
        
        return codeableConcept
    }
    
    /// Generates a coding object for the observation resource
    ///
    /// - Parameters:
    ///   - withSystem: the value of the system for the coding
    ///   - withCode: the value of the code for the coding
    ///   - withDisplay: the display value for the coding
    /// - Returns: the coding object
    private func getCoding(withSystem:String, withCode:String, withDisplay:String) -> Coding{
        let coding = Coding()
        coding.system = FHIRURL.init(withSystem)
        coding.code = FHIRString.init(withCode)
        coding.display = FHIRString.init(withDisplay)
        
        return coding
    }
    
    /// Generates a value quantity object for the observation resource
    ///
    /// - Parameters:
    ///   - heartRate: the heart rate value
    ///   - unit: the unit of the heart rate value
    ///   - system: the value of the system for the value quantity
    ///   - code: the value of the code for the value quantity
    /// - Returns: the value quantity object
    private func getValueQuantity(heartRate: String, unit:String, system:String, code:String) -> Quantity{
        let quantity = Quantity()
        quantity.value = FHIRDecimal.init(Decimal(string: heartRate)!)
        quantity.unit = FHIRString.init(unit)
        quantity.system = FHIRURL.init(system)
        quantity.code = FHIRString.init(code)
        
        return quantity
    }
    
    /// Generates a identifer object for the observation resource
    ///
    /// - Parameters:
    ///   - withValue: the value of the identifer
    /// - Returns: the identifer object
    private func getIdentifer(withValue: String) -> Identifier{
        let identifer = Identifier()
        identifer.value = FHIRString.init(withValue)
        
        return identifer
    }
    
    /// Generates a meta object for the observation resource
    ///
    /// - Parameters:
    ///   - with profile: the usd profile value for the observation resource
    /// - Returns: the meta object
    private func getMeta(withProfile:String) -> Meta{
        let meta = Meta()
        meta.profile = [FHIRURL.init(withProfile)] as? [FHIRURL]
        
        return meta
    }
    
    /// Generates a reference object for the observation resource
    ///
    /// - Parameters:
    ///   - withPid: the patient identifer
    /// - Returns: the reference object
    private func getReference(withPid:String) -> Reference {
        let reference = Reference()
        reference.reference = FHIRString.init(withPid)
        
        return reference
    }
    
    /// Fallback if the SMART on FHIR library doesn't work.
    /// Generates an observation resource as simple XML string
    ///
    /// - Parameters:
    /// - Parameters:
    ///   - withPid: the patient identifier
    ///   - withDate: the dateTime when the observation was made
    ///   - withHeartRate: the heart reate value
    ///   - withSteps: the steps value
    /// - Returns: the observation resource as xml string
    func getObservationXML(withPid: String, withDate: String, withHeartRate: String, withSteps: String) -> String{
        
        return "<Observation xmlns='http://hl7.org/fhir'><id value='heart-rate'/><meta><profile value='http://hl7.org/fhir/StructureDefinition/vitalsigns'/></meta><status value='final'/><category><coding><system value='http://terminology.hl7.org/CodeSystem/observation-category'/><code value='vital-signs'/><display value='Vital Signs'/></coding><text value='Vital Signs'/></category><code><coding><system value='http://loinc.org'/><code value='8867-4'/><display value='Heart rate'/></coding><text value='Heart rate'/></code><subject><reference value='"+withPid+"'/></subject><effectiveDateTime value='"+withDate+"'/><valueQuantity><value value='"+withHeartRate+"'/><unit value='beats/minute'/><system value='http://unitsofmeasure.org'/><code value='/min'/></valueQuantity><comment value='"+withSteps+"' /></Observation>"
    }
}
