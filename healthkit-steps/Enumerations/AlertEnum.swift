import Foundation

/// string constants with alert context
enum AlertEnum:String {
    case title_error = "Error"
    case title_success = "Success"
    case title_info = "Info"
    case message_error_internal = "An error occurred while processing!"
    case message_error_server = "The server request was not successful!"
    case message_success = "Your data was successfully pushed!"
    case message_info_uptodate = "Your data is up to date!"
}
