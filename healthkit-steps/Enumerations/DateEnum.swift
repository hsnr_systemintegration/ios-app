import Foundation

/// string constants with date context
enum DateEnum:String {
    case pattern_long = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
}
