import Foundation

/// String constants with logging context
struct LoggingStrings {

    static let OBSERVATION_CREATED:StaticString = "Creates a new observation resource for patient with id '%@' and heart rate (%@) from %@. Todays steps are '%@'."
    static let REQUEST_SUCCESS:StaticString = "The request with observation %@ has successfully submitted."
    static let REQUEST_ERROR_INTERNAL:StaticString = "Something went wrong. The request was not able to be sent. An internal error occurred."
    static let REQUEST_ERROR_CODE:StaticString = "Something went wrong. The request was not able to be sent. Server returned no success."
    static let AUTHORIZATION_FAILED:StaticString = "oops something went wrong during authorisation %@"
    static let AUTHORIZATION_SUCCESS:StaticString = "User has completed the authorization flow"
    
    private init() {}
}
