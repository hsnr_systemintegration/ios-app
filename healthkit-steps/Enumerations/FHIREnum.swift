import Foundation

/// String constants with FHIR context
enum FHIREnum:String {
    case category_coding_system = "http://terminology.hl7.org/CodeSystem/observation-category"
    case category_coding_code = "vital-signs"
    case category_coding_display = "Vital Signs"
    case code_coding_system = "http://loinc.org"
    case code_coding_code = "8867-4"
    case code_coding_display = "Heart Rate"
    case valueQuantity_system = "http://unitsofmeasure.org"
    case valueQuantity_code = "/min"
    case valueQuantity_unit = "beats/minute"
    case identifer = "sde-observation-resource"
    case meta_profile = "http://hl7.org/fhir/StructureDefinition/vitalsigns"
}
