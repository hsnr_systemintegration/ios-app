import HealthKit
import UIKit
import Foundation


/// the main view controller of the app
class ViewController: UIViewController {
    
    @IBOutlet weak var serverField: UITextField!
    @IBOutlet weak var pidField: UITextField!
    @IBOutlet weak var pushBtn: UIButton!
    
    let vitalsService = VitalsService(healthStore: HKHealthStore())
    
    /// Called when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        inputStorage()
        enablePushBtn()
        vitalsService.authoriseHealthKitAccess()
    }
    
    /// Necessary for fetching warnings
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// push action for the push data button. Starts the progess of the app
    ///
    /// - Parameter sender: the push data button
    @IBAction func pushBtn(_ sender: UIButton) {
        
        vitalsService.fetchVitals(viewController: self)
    }
    
    /// OnFieldChanged action for the server textfield. Sets the value to the local storage
    ///
    /// - Parameter sender: the server textfield
    @IBAction func onServerFieldChanged(_ sender: UITextField) {
        UserDefaults.standard.set(sender.text!, forKey: "server")
        enablePushBtn()
    }
    
    /// OnFieldChanged action for the pid textfield. Sets the value to the local storage
    ///
    /// - Parameter sender: the pid textfield
    @IBAction func onPidFieldChanged(_ sender: UITextField) {
        UserDefaults.standard.set(sender.text!, forKey: "pid")
        enablePushBtn()
    }
    
    /// Writes the value of the local storage to the textfields
    func inputStorage(){
        pidField.text = UserDefaults.standard.string(forKey: "pid")
        serverField.text = UserDefaults.standard.string(forKey: "server")
    }
    
    /// Enables the push button when the textfields are filled
    func enablePushBtn(){
        if(pidField.text != "" && serverField.text != ""){
            pushBtn.isEnabled = true
        } else {
            pushBtn.isEnabled = false
        }
    }
}

